import { Client, TextChannel, MessageEmbed } from 'discord.js'
import WOKCommands from 'wokcommands'
import banSchema from '../models/ban-schema';

export default async (client: Client, instance: WOKCommands) => {
    client.on('guildCreate', async (guild) => {
        try {
            const channel = guild.channels.cache.find(channel => channel.type === 'text' && channel.permissionsFor(guild.me).has('SEND_MESSAGES'))

            const welcomeEmbed = new MessageEmbed()
                .setTitle('Welcome to CrossBan!')
                .setDescription('Thanks for adding CrossBan to your server, now I need to explain a few things.')
                .addField('1. Make sure ALL staff members join the support server.', 'Due to the nature the bot exists in, there is a small list of "*permitted users*" that can ban people across Discord servers. Make sure staff members join so they can request a global ban where our team will identify the problem and take it from there.\nhttps://discord.gg/FJBcNT6zbF')
                .addField('~~2. Run /refresh.', 'This is a command that will make the bot run through the global ban list and ban anyone that matches. This is a good way to make sure the bot is up to date.~~')
                .setFooter('CrossBan | Making Discord Safer')

            channel.send({embed: welcomeEmbed})
        } catch {
            console.log(`
            Error!
            Unable to send message to channel. Contact owner.
            `)
        }
    });
}
